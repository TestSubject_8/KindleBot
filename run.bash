#!/bin/bash

if [ ! -e ~/.config/KindleBot/details.txt ]
then
	mkdir ~/.config/KindleBot
	touch ~/.config/KindleBot/details.txt
	mkdir ~/Documents/Conv_PDFs
	mkdir ~/DocumentsSaved_PDFs
	python config.py
fi

python emaillistener.py

if [ ! $?=0 ]
then
	exit
fi

for file in ~/Documents/Saved_PDFs/*;
do
	loc=~/Documents/Conv_PDFs/
	nname=${file##*/}
	name=${file/.pdf/.mobi}
	nfile=$loc$name
	ebook-convert $file $nfile
	if [ ! $?=0 ]
	then
		continue
	fi
	rm $file
done

python sendemail.py

if [ ! $?=0 ]
then
	exit
fi

for file in ~/Documents/Conv_PDFs/*;
do
	rm $file
done